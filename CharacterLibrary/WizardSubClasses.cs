﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterLibrary
{
    public class HogwartsWizard : Wizard
    {
        public HogwartsWizard(string name, int hp, int energy, int armorRating) : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } swings his wand<3";
        }

        public override string Move()
        {
            return $"{ Name } hangs around wondering what the meaning of life is" +
                $" when the only thing you have to do to get forward in life is to swing your wand around";
        }
    }

    public class Wicca : Wizard
    {
        public Wicca(string name, int hp, int energy, int armorRating) : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } attacks an monotheist";
        }

        public override string Move()
        {
            return $"{ Name } goes to a giant orgie";
        }
    }
}

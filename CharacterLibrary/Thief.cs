﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterLibrary
{
    public abstract class Thief : Character
    {
        protected Thief(string name, int hp, 
            int energy, int armorRating) 
            : base(name, hp, energy, armorRating)
        {
        }
    }
}

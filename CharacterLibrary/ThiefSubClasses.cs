﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterLibrary
{
    public class CarThief : Thief
    {
        public CarThief(string name, int hp, 
            int energy, int armorRating) 
            : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } damages the pocket of his opponent by stealing his Tesla";
        }

        public override string Move()
        {
            return $"{ Name } sneaks around looking for cars";
        }
    }

    public class Pirate : Thief
    {
        public Pirate(string name, int hp,
            int energy, int armorRating)
            : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } fires his cannons!";
        }

        public override string Move()
        {
            return $"{ Name } roam the seven seas in his 300HP rib";
        }
    }

    public class WhiteCollar : Thief
    {
        public WhiteCollar(string name, int hp,
            int energy, int armorRating)
            : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } seems very friendly";
        }

        public override string Move()
        {
            return $"{ Name } drives away in a sports car";
        }
    }
}

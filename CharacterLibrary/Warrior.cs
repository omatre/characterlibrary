﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterLibrary
{
    public abstract class Warrior : Character
    {
        protected Warrior(string name, int hp, 
            int energy, int armorRating) 
            : base(name, hp, energy, armorRating)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CharacterLibrary
{
    public class GIJoe : Warrior
    {
        public GIJoe(string name, int hp, int energy, int armorRating) : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } fires a round seemingly without aiming";
        }

        public override string Move()
        {
            return $"{ Name } sneak around waiting to be ambushed";
        }
    }

    public class Viking : Warrior
    {
        public Viking(string name, int hp, int energy, int armorRating) : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } launches a bat shit cracy attack from his boat";
        }

        public override string Move()
        {
            return $"{ Name } works the oars";
        }
    }

    public class KeyboardWarrior : Warrior
    {
        public KeyboardWarrior(string name, int hp, int energy, int armorRating) : base(name, hp, energy, armorRating)
        {
        }

        public override string Attack()
        {
            return $"{ Name } angrily launches an attack calling everyone else snowflakes";
        }

        public override string Move()
        {
            return $"{ Name } barely moves and stays inside a lot";
        }
    }
}

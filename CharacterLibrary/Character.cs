﻿using System;

namespace CharacterLibrary
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int HP { get; set; }
        public int Energy { get; set; }
        public int ArmorRating { get; set; }

        public Character(string name, int hp, 
            int energy, int armorRating)
        {
            Name = name;
            HP = hp;
            Energy = energy;
            ArmorRating = armorRating;
        }

        public abstract string Attack();
        public abstract string Move();
    }
}
